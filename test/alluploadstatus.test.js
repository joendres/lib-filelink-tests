import { allUploadStatus } from "../lib-filelink/allUploadStatus.js";

describe("allUploadStatus", () => {
    it("should start empty", () => {
        expect(allUploadStatus.size).to.be(0);
    });
    it("should start with port=null", () => {
        expect(allUploadStatus.port).to.be(null);
    });

    describe("update", () => { });

    describe("set", () => { });
});
