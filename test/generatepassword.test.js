import { generatePassword } from "../lib-filelink/generatepassword.js";

describe("generatePassword", () => {
    it("should return a string at least 12 characters long if called with no options", () => {
        expect(generatePassword().length).to.be.greaterThan(11);
    });
    it("should only contain characters between 0x21 and 0x7e if called with no options", () => {
        expect(generatePassword()).to.match(/^[\x21-\x7e]+$/);
    });
    it("should contain a lower case letter if called with no options", () => {
        expect(generatePassword()).to.match(/[a-z]/);
    });
    it("should contain an upper case letter if called with no options", () => {
        expect(generatePassword()).to.match(/[A-Z]/);
    });
    it("should contain a number if called with no options", () => {
        expect(generatePassword()).to.match(/\d/);
    });
    it("should contain a punctuation character if called with no options", () => {
        expect(generatePassword()).to.match(/[^a-zA-Z\d]/);
    });
    it("should return a string at least 120 characters long if called with 120 as length option", () => {
        expect(generatePassword(120).length).to.be.greaterThan(119);
    });

    it("should throw an error if called with contradictory options", () => {
        expect(generatePassword).withArgs(12, "0123456789").to.throwException();
    });

    it("should not return a string containing excluded letters", () => {
        expect(generatePassword(10000, "a")).to.match(/^[^a]+$/);
    });
});

/* global describe,it,expect */