import { Localizer } from "../lib-filelink/localizer.js";

describe("Localizer", () => {
    describe("localizeManifestString", () => {
        it("should substitute a locale if there is one", () => {
            expect(Localizer.localizeManifestString("__MSG_Test__")).to.be("Hello world!");
        });
        it("should return the string as is if there is no substitution", () => {
            expect(Localizer.localizeManifestString("__MSG_NotThere__")).to.be("__MSG_NotThere__");
        });
        it("should return the string as is if it is not a template", () => {
            expect(Localizer.localizeManifestString("Hello world!")).to.be("Hello world!");
        });
        it("should return the string as is if it has more than the template", () => {
            expect(Localizer.localizeManifestString("Hello __MSG_NotThere__!")).to.be("Hello __MSG_NotThere__!");
        });
    });
    describe("localizeLabels", () => {
        Localizer.localizeLabels();
        it("should put a locale text into an element", () => {
            expect(document.querySelector("#localizeLabels_1").textContent).to.be("Hello world!");
        });
        it("should put a template text into an element if there is no replacement", () => {
            expect(document.querySelector("#localizeLabels_2").textContent).to.be("__MSG_Missing__");
        });
    });
});
